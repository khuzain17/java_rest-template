package com.example.restapi.latihan10resttemplate.controller;

import com.example.restapi.latihan10resttemplate.entity.EmployeeTraining;
import com.example.restapi.latihan10resttemplate.repository.EmployeeTrainingRepo;
import com.example.restapi.latihan10resttemplate.service.EmployeeTrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("synrgy/employee-training")
public class EmployeeTrainingController {
    @Autowired
    EmployeeTrainingService employeeTrainingService;

    @Autowired
    public EmployeeTrainingRepo repo;

    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<Map> getAll() {
        Map employees = employeeTrainingService.getAll();
        return new ResponseEntity<Map>(employees, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody EmployeeTraining newEmployeeTraining) {
        Map map = new HashMap();
        Map emp = employeeTrainingService.insert(newEmployeeTraining);

        map.put("Request = ", newEmployeeTraining);
        map.put("Response = ", emp);
        return new ResponseEntity<Map>(emp, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody EmployeeTraining employeeTrainingToUpdate) {
        Map map = new HashMap();
        Map emp = employeeTrainingService.update(employeeTrainingToUpdate);

        map.put("Request = ", employeeTrainingToUpdate);
        map.put("Response = ", emp);
        return new ResponseEntity<Map>(emp, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id) {
        Map emp = employeeTrainingService.delete(id);
        return new ResponseEntity<Map>(emp, HttpStatus.OK);
    }

}
