package com.example.restapi.latihan10resttemplate.service;

import com.example.restapi.latihan10resttemplate.entity.EmployeeTraining;

import java.util.Map;

public interface EmployeeTrainingService {
    public Map insert(EmployeeTraining employeeTraining);

    public Map update(EmployeeTraining employeeTraining);

    public Map delete(Long idemployeeTraining);

    public Map getAll();
}
