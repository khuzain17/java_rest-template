package com.example.restapi.latihan10resttemplate.service;

import com.example.restapi.latihan10resttemplate.entity.Rekening;

import java.util.Map;

public interface RekeningService {
    public Map insert(Rekening rekening, Long idKaryawan);

    public Map update(Rekening rekening);

    public Map delete(Long idRekening);

    public Map getById(Long idRekening);

    public Map getAll();
}
