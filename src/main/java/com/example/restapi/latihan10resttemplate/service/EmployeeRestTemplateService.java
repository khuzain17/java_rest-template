package com.example.restapi.latihan10resttemplate.service;

import com.example.restapi.latihan10resttemplate.entity.Employee;

import java.util.Map;

public interface EmployeeRestTemplateService {
    public Map insert(Employee employee);// request lempar objek

    public Map update(Employee employee); //DI objek request

    public Map delete(Long idemployee);// delete by id

    public Map getData(); // nampilin semua data

    public Map getById(Long idemployee);
}
