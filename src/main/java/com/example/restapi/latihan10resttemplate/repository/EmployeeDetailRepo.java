package com.example.restapi.latihan10resttemplate.repository;

import com.example.restapi.latihan10resttemplate.entity.EmployeeDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeDetailRepo extends JpaRepository<EmployeeDetail, Long> {
    @Query("select e from EmployeeDetail e where e.id = :id")
    public EmployeeDetail getById(@Param("id") Long id);

    @Query("select e from EmployeeDetail e")
    public List<EmployeeDetail> getAllEmployeeDetail();
}
